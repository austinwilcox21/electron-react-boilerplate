# Start instructions
In one terminal window run 
```bash
npm run watch
```
This will allow webpack/babel to compile the javascript to something that is readable with the browser.

Then in the next terminal window run
```bash
npm run start
```

This will open a window with your react application.
