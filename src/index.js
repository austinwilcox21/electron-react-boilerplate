import React from 'react';
import ReactDOM from 'react-dom';
import Component from './components/component.js'

ReactDOM.render(<Component />, document.getElementById('root'));
